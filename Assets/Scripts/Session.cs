﻿using System;

[Serializable]
public class Session : IRestartable
{
	public event Action<int> OnBonusPickUp;

	public int BonusCount { get; private set; }

	public void Restart()
	{
		BonusCount = 0;
		OnBonusPickUp(BonusCount);
	}

	public void BonusPicked()
	{
		BonusCount++;
		OnBonusPickUp(BonusCount);
	}
}
