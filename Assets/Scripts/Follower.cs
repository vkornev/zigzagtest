﻿using UnityEngine;

public class Follower : MonoBehaviour, IRestartable
{
	[SerializeField] private Transform target = null;
	[SerializeField] private GameController gameCtrl = null;

	private Vector3 initialDirection;
	private Vector3 startPosition;

	public void Restart()
	{
		transform.position = startPosition;
	}

	private void Start()
	{
		initialDirection = transform.forward.normalized;
		startPosition = transform.position;
	}

	private void LateUpdate()
    {
		float chaseSpeed = transform.InverseTransformPoint(target.position).z;
		transform.position = Vector3.Lerp(transform.position, transform.position + initialDirection, chaseSpeed * Time.deltaTime);
    }

	private void OnTriggerEnter(Collider other)
	{
		if (!other.CompareTag("Player")) return;
		gameCtrl.Restart();
	}
}
