﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public abstract class BonusGenerator : ScriptableObject, IBonusGenerator, IRestartable
{
	public Action OnBonusPickUp;

	[SerializeField] protected GameObject bonus;
	[SerializeField] protected int bonusPoolLimit;
	[SerializeField] protected Vector3 positionShift;
	[SerializeField] protected int chunkSize = 5;

	protected Queue<GameObject> bonusPool;

	public abstract GameObject Generate(Vector3 place);

	public virtual void Init(Transform parent, Session session)
	{
		bonusPool = new Queue<GameObject>();

		for (int i = 0; i < bonusPoolLimit; i++)
		{
			GameObject bonusObj = Instantiate(bonus, Vector3.zero, Quaternion.identity);
			bonusObj.transform.parent = parent;
			bonusObj.SetActive(false);
			bonusObj.GetComponent<Bonus>().Init(session);
			bonusPool.Enqueue(bonusObj);
		}
	}

	public void Restart()
	{
		for (int i = 0; i < bonusPool.Count; i++)
		{
			GameObject bonusObj = bonusPool.Dequeue();
			bonusObj.SetActive(false);
			bonusPool.Enqueue(bonusObj);
		}
	}

	public void ResetBonus(GameObject bonus)
	{
		bonus.SetActive(false);
	}

	protected GameObject PickFromPool(Vector3 place)
	{
		GameObject result = bonusPool.Dequeue();
		result.transform.position = place + positionShift;
		result.SetActive(true);
		bonusPool.Enqueue(result);
		return result;
	}
}
