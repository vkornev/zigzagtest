﻿using UnityEngine;

public class Bonus : MonoBehaviour
{
	private Session session;

	public void Init(Session session)
	{
		this.session = session;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (!other.CompareTag("Player")) return;

		session.BonusPicked();
		gameObject.SetActive(false);
	}
}
