﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour, IRestartable
{
	[SerializeField] private GameObject tile = null;
	[SerializeField] private GameController settings = null;
	[SerializeField] private Camera activeCamera = null;
	[SerializeField] private float leftLimit = 0f;
	[SerializeField] private float rightLimit = 1f;
	[SerializeField] private float topLimit = 1f;

	private delegate GameObject GetPoolObject();
	private Action<Vector3> buildSegment;
	private Queue<GameObject> tilePool = new Queue<GameObject>();
	private Vector3 lastTilePos = Vector3.zero;
	private int tileSegmentSideLegth;

	public void Restart()
    {
		StopAllCoroutines();
		ManualStart(ExistingPoolObject);
	}

	public Vector3 GenerateTerrain()
	{
		Vector3 viewportPos = activeCamera.WorldToViewportPoint(lastTilePos);
		bool goForward = UnityEngine.Random.Range(0f, 1f) >= .5f;
		if (viewportPos.x >= rightLimit)
			goForward = true;
		else if (viewportPos.x <= leftLimit)
			goForward = false;

		if (viewportPos.y >= topLimit) return Vector3.negativeInfinity;

		Vector3 newPos = lastTilePos + (goForward ? Vector3.forward : Vector3.right) * tileSegmentSideLegth;
		lastTilePos = newPos;

		buildSegment(newPos);

		return newPos;
	}

	public void ResetTile(GameObject tile)
	{
		StartCoroutine(DropTile(tile));
	}

	private void Start()
	{
		ManualStart(NewPoolObject);
	}

	private GameObject NewPoolObject()
	{
		return Instantiate(tile, Vector3.zero, Quaternion.identity);
	}

	private GameObject ExistingPoolObject()
	{
		return tilePool.Dequeue();
	}

	private void ManualStart(GetPoolObject getObject)
	{
		lastTilePos = Vector3.zero;

		tileSegmentSideLegth = (3 - (int)settings.Difficulty);
		switch (tileSegmentSideLegth)
		{
			case 1:
				buildSegment = PickFromPool;
				break;
			case 2:
				buildSegment = Build2TileSegment;
				break;
			case 3:
				buildSegment = Build3TileSegment;
				break;
		}

		//limit the pool by the size of camera FOV and the number of tiles in a segment
		int tilePoolLimit = (int)(activeCamera.transform.position.y * Mathf.Tan(Mathf.Deg2Rad * activeCamera.fieldOfView) * 4f / Mathf.Sqrt(2)) *
			tileSegmentSideLegth;

		for (int i = 0; i < tilePoolLimit; i++)
		{
			GameObject tileObj = getObject();
			tileObj.transform.parent = transform;
			tileObj.SetActive(false);
			tilePool.Enqueue(tileObj);
		}

		Build3TileSegment(lastTilePos);
	}

	private void Build2TileSegment(Vector3 startPos)
	{
		for (int x = (int)startPos.x - 1; x <= startPos.x; x++)
			for (int z = (int)startPos.z - 1; z <= startPos.z; z++)
				PickFromPool(new Vector3(x, 0, z));
	}

	private void Build3TileSegment(Vector3 startPos)
	{
		for (int x = (int)startPos.x - 1; x <= startPos.x + 1; x++)
			for (int z = (int)startPos.z - 1; z <= startPos.z + 1; z++)
				PickFromPool(new Vector3(x, 0, z));
	}

	private void PickFromPool(Vector3 tilePos)
	{
		GameObject tile = tilePool.Dequeue();
		tile.transform.position = tilePos;
		tile.SetActive(true);
		tilePool.Enqueue(tile);
	}

	private IEnumerator DropTile(GameObject tile)
	{
		float t = 1.5f;
		while (t > 0)
		{
			float deltaTime = Time.deltaTime;
			tile.transform.localPosition = Vector3.Lerp(tile.transform.localPosition, tile.transform.localPosition + Vector3.down, deltaTime * 5f);
			t -= deltaTime;
			yield return new WaitForEndOfFrame();
		}

		tile.SetActive(false);
	}
}
