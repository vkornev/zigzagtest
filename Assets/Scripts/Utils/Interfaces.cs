﻿using UnityEngine;

public interface IRestartable
{
	void Restart();
}

public interface IBonusGenerator
{
	void Init(Transform parent, Session session);
	GameObject Generate(Vector3 place);
}