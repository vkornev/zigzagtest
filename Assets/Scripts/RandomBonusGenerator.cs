﻿using UnityEngine;

[CreateAssetMenu]
public class RandomBonusGenerator : BonusGenerator
{
	[SerializeField] protected float bonusChance;

	private int tileCounter = 0;
	private int bonusTileId = 0;

	public override void Init(Transform parent, Session session)
	{
		base.Init(parent, session);
		GenerateBonusId();
	}

	public override GameObject Generate(Vector3 place)
	{
		GameObject result = null;

		if (bonusTileId == tileCounter)
			result = PickFromPool(place);

		tileCounter++;

		if (tileCounter >= chunkSize)
		{
			tileCounter = 0;
			GenerateBonusId();
		}

		return result;
	}

	private void GenerateBonusId()
	{
		bonusTileId = Random.Range(0, chunkSize);
	}
}
