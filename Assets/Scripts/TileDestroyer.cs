﻿using UnityEngine;

public class TileDestroyer : MonoBehaviour
{
	[SerializeField] private GameController gameCtrl = null;

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Tile"))
			gameCtrl.TerrainGenerator.ResetTile(other.gameObject);

		if (other.CompareTag("Bonus"))
			gameCtrl.BonusGenerator.ResetBonus(other.gameObject);
	}
}
