﻿using UnityEngine;

[CreateAssetMenu]
public class LogicalBonusGenerator : BonusGenerator
{
	private int chunkCounter = 0;
	private int tileCounter = 0;

	public override GameObject Generate(Vector3 place)
	{
		GameObject result = null;

		//То есть на первом блоке - 1-ый тайл с кристаллом, на 2-ом - 2 тайл и так далее до 5-ого блока. Далее опять с 1-ого по 5.
		int bonusTileId = chunkCounter % chunkSize;

		if (bonusTileId == tileCounter)
			result = PickFromPool(place);

		tileCounter++;

		if (tileCounter >= chunkSize)
		{
			tileCounter = 0;
			chunkCounter++;
		}

		return result;
	}
}
