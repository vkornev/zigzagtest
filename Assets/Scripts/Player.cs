﻿using UnityEngine;

public class Player : MonoBehaviour, IRestartable
{
	[SerializeField] private float speed = 0f;

	private Rigidbody myRigidbody;
	private Vector3 moveDelta;
	private bool goForward = true;
	private Vector3 startPosition;

	public void Restart()
	{
		transform.position = startPosition;
		moveDelta = Physics.gravity;
		goForward = true;
	}

	private void Start()
	{
		myRigidbody = GetComponent<Rigidbody>();
		moveDelta = Physics.gravity;
		startPosition = transform.position;
	}

	private void Update()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			goForward = !goForward;
			moveDelta = (goForward ? Vector3.forward : Vector3.right) + Physics.gravity;
		}
	}

	private void FixedUpdate()
	{
		myRigidbody.velocity = moveDelta * speed;
	}
}
