﻿using UnityEngine;
using TMPro;

public class UIBonus : MonoBehaviour
{
	[SerializeField] GameController gameCtrl = null;
	[SerializeField] TextMeshProUGUI bonusCount = null;

	private void OnEnable()
	{
		gameCtrl.Session.OnBonusPickUp += UpdateBonus;
	}

	private void OnDisable()
	{
		gameCtrl.Session.OnBonusPickUp -= UpdateBonus;
	}

	private void UpdateBonus(int bonus)
	{
		bonusCount.text = bonus.ToString();
	}
}
