﻿using UnityEngine;

public enum Difficulty { Easy, Normal, Hard }

public class GameController : MonoBehaviour, IRestartable
{
	public Difficulty Difficulty { get { return difficulty; } }
	public Session Session { get; set; } = new Session();
	public BonusGenerator BonusGenerator { get => bonusGenerator; }
	public TerrainGenerator TerrainGenerator { get => terrainGenerator; }

	[SerializeField] private Difficulty difficulty = Difficulty.Easy;
	[SerializeField] private TerrainGenerator terrainGenerator = null;
	//change the scriptable object to change the logic of bonus generation.
	[SerializeField] private BonusGenerator bonusGenerator = null;
	[SerializeField] private Player player = null;
	[SerializeField] private Follower cameraFollower = null;

	public void Restart()
	{
		cameraFollower.Restart();
		Session.Restart();
		bonusGenerator.Restart();
		terrainGenerator.Restart();
		player.Restart();
	}

	private void Start()
	{
		Session.Restart();

		bonusGenerator.Init(terrainGenerator.transform, Session);
	}

	private void Update()
	{
		Vector3 tilePos = terrainGenerator.GenerateTerrain();
		if (tilePos.x > -Mathf.Infinity)
			bonusGenerator.Generate(tilePos);
	}
}
